<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller']   = 'aplikasi';
$route['404_override']         = 'aplikasi/error';
$route['translate_uri_dashes'] = false;

$route['index']  = 'aplikasi/index';
$route['login']  = 'aplikasi/login';
$route['logout'] = 'aplikasi/logout';
$route['lupa']   = 'aplikasi/lupa';

$route['profil'] = 'aplikasi/profil';

$route['akun']             = 'aplikasi/akun';
$route['akun/tambah']      = 'aplikasi/akun_tambah';
$route['akun/ubah/(:any)'] = 'aplikasi/akun_ubah/$1';

$route['pemasok']               = 'aplikasi/pemasok';
$route['pemasok/tambah']        = 'aplikasi/pemasok_tambah';
$route['pemasok/ubah/(:any)']   = 'aplikasi/pemasok_ubah/$1';
$route['pemasok/detail/(:any)'] = 'aplikasi/pemasok_detail/$1';

$route['kategori']               = 'aplikasi/kategori';
$route['kategori/tambah']        = 'aplikasi/kategori_tambah';
$route['kategori/ubah/(:any)']   = 'aplikasi/kategori_ubah/$1';
$route['kategori/detail/(:any)'] = 'aplikasi/kategori_detail/$1';

$route['cek_login']            = 'aksi/cek_login';
$route['cek_lupa']             = 'aksi/cek_lupa';
$route['aksi/profil/ubah']     = 'aksi/profil_ubah';
$route['aksi/akun/tambah']     = 'aksi/akun_tambah';
$route['aksi/akun/ubah']       = 'aksi/akun_ubah';
$route['aksi/pemasok/tambah']  = 'aksi/pemasok_tambah';
$route['aksi/pemasok/ubah']    = 'aksi/pemasok_ubah';
$route['aksi/kategori/tambah'] = 'aksi/kategori_tambah';
$route['aksi/kategori/ubah']   = 'aksi/kategori_ubah';
