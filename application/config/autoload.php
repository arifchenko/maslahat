<?php
defined('BASEPATH') or exit('No direct script access allowed');
$autoload['packages']  = array();
$autoload['libraries'] = array('database', 'session', 'form_validation', 'user_agent');
$autoload['drivers']   = array();
$autoload['helper']    = array('url', 'form', 'security', 'file', 'date');
$autoload['config']    = array();
$autoload['language']  = array();
$autoload['model']     = array('fungsi', 'data');
