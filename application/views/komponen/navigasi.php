<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo mr-5" href="<?php echo base_url('index'); ?>"><img src="<?php echo base_url('assets/images/maslahat-motor.png'); ?>" class="ml-3" alt="logo"/></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
    <ul class="navbar-nav">
      <li class="nav-item d-none d-lg-block ml-0">
        <h5 class="mb-0" id="display-datetime">
          <?php
            setlocale(LC_TIME, 'IND');
            echo strftime('%A, %e %B %G - %H:%M:%S');
          ?>
        </h5>
      </li>
    </ul>
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item">
        <h4 class="mb-0"><?php echo $akun->nama; ?></h4>
      </li>
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
          <img src="<?php echo !is_null($akun->foto) ? base_url('assets/images/akun/' . $akun->foto) : base_url('assets/images/avatar.jpg'); ?>" alt="profile"/>
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
          <a class="dropdown-item" href="<?php echo base_url('profil'); ?>">
            <i class="ti-settings text-primary"></i>
            Profil
          </a>
          <a class="dropdown-item" href="<?php echo base_url('logout'); ?>">
            <i class="ti-power-off text-primary"></i>
            Logout
          </a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="icon-menu"></span>
    </button>
  </div>
</nav>