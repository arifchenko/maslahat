<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url('index'); ?>">
        <i class="icon-home menu-icon"></i>
        <span class="menu-title">Beranda</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=<?php echo base_url('kasir'); ?>>
        <i class="icon-monitor menu-icon"></i>
        <span class="menu-title">Kasir</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=<?php echo base_url('pembelian'); ?>>
        <i class="icon-truck menu-icon"></i>
        <span class="menu-title">Pembelian</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#basisdata" aria-expanded="false" aria-controls="basisdata">
        <i class="icon-database menu-icon"></i>
        <span class="menu-title">Basis Data</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="basisdata">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('barang'); ?>">Barang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('kategori'); ?>">Kategori</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('pemasok'); ?>">Pemasok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('pelanggan'); ?>">Pelanggan</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#laporan" aria-expanded="false" aria-controls="laporan">
        <i class="icon-clipboard menu-icon"></i>
        <span class="menu-title">Laporan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="laporan">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('laporan/kas'); ?>">Kas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('laporan/gaji'); ?>">Gaji</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('laporan/penjualan'); ?>">Penjualan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('laporan/pembelian'); ?>">Pembelian</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('laporan/customer'); ?>">Customer</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=<?php echo base_url('akun'); ?>>
        <i class="icon-users menu-icon"></i>
        <span class="menu-title">Akun</span>
      </a>
    </li>
  </ul>
</nav>