<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">
                  <a href="<?php echo base_url('kategori'); ?>">Tabel Kategori</a>
                  <i class="menu-icon icon-chevron-right"></i>
                  Tambah
                </h3>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <form class="forms-sample" method="post" action="<?php echo base_url('aksi/kategori/tambah'); ?>">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" name="nama" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-info mr-2">Simpan</button>
                    <a href="<?php echo base_url('kategori'); ?>" class="btn btn-light">Kembali</a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

