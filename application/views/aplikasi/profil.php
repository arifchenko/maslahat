<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">Profil</h3>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <form class="forms-sample" method="post" action="<?php echo base_url('aksi/profil/ubah'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                      <label>Foto</label>
                      <input type="file" name="foto" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" name="nama" class="form-control" value="<?php echo $profil->nama; ?>" required>
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="email" class="form-control" value="<?php echo $profil->email; ?>" required>
                    </div>
                    <div class="form-group">
                      <label>HP</label>
                      <input type="text" name="hp" class="form-control" value="<?php echo $profil->hp; ?>" required>
                    </div>
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" name="username" class="form-control" value="<?php echo $profil->username; ?>" required>
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" name="password" class="form-control" placeholder="Kosongkan bila tidak berubah">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

