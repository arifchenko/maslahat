<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Maslahat Motor</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/feather/feather.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/ti-icons/css/themify-icons.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/vertical-layout-light/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/maslahat-icon.png'); ?>" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="<?php echo base_url('assets/images/maslahat-logo.png'); ?>" alt="logo">
              </div>
              <h6 class="font-weight-light">Silakan masukkan email</h6>
              <form class="pt-3" method="post" action="<?php echo base_url('cek_lupa'); ?>">
                <div class="form-group">
                  <input type="text" name="email" class="form-control form-control-lg" placeholder="Email" required>
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">KIRIM</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url('assets/vendors/js/vendor.bundle.base.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/off-canvas.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/hoverable-collapse.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/template.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/settings.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/todolist.js'); ?>"></script>
</body>

</html>
