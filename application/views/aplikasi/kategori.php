<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">Tabel Kategori</h3>
                <a href="<?php echo base_url('kategori/tambah'); ?>" class="btn btn-info btn-icon-text btn-sm">
                  <i class="menu-icon icon-plus-circle btn-icon-prepend"></i>
                  Tambah
                </a>
              </div>
              <div class="card">
                <div class="card-body">
                  <div class="filter">
                    <div class="input-group input-group-sm" style="width:250px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-search"></i>
                        </span>
                      </div>
                      <input type="text" name="cari" class="form-control" placeholder="Cari kategori" id="cari-tabel" onkeyup="cariTabel();" autofocus>
                    </div>
                    <div class="input-group input-group-sm" style="width:150px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-arrow-down"></i>
                        </span>
                      </div>
                      <select name="urutan" class="form-control" onchange="urutanTabel(this.value);">
                        <option value="1">Nama</option>
                        <option value="2">Jumlah</option>
                      </select>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped" id="tabel-data">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Jumlah</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="tbody">
                        <?php
                          if (!is_null($tabel_kategori)) {
                            $nomor = 1;
                            foreach ($tabel_kategori as $kategori) {
                        ?>
                        <tr>
                          <td><?php echo $nomor++; ?>.</td>
                          <td><?php echo $kategori->nama; ?></td>
                          <td><?php echo $kategori->jumlah; ?></td>
                          <td align="right">
                            <a class="ml-2 text-primary" href="<?php echo base_url("kategori/detail/" . $kategori->id); ?>"><i class="icon-file-text menu-icon"></i> Detail</a>
                            <a class="ml-2 text-success" href="<?php echo base_url("kategori/ubah/" . $kategori->id); ?>"><i class="icon-edit-3 menu-icon"></i> Ubah</a>
                          </td>
                        </tr>
                        <?php
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

