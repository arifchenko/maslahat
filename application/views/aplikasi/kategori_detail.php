<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">
                  <a href="<?php echo base_url('kategori'); ?>">Tabel Kategori</a>
                  <i class="menu-icon icon-chevron-right"></i>
                  Detail
                  <i class="menu-icon icon-chevron-right"></i>
                  <?php echo $detail_kategori->nama; ?>
                </h3>
                <a href="<?php echo base_url('kategori/ubah/' . $detail_kategori->id); ?>" class="btn btn-success btn-icon-text btn-sm">
                  <i class="menu-icon icon-edit-2 btn-icon-prepend"></i>
                  Ubah
                </a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $detail_kategori->nama; ?></h4>
                  <p class="card-description">
                    <span class="card-label">Jumlah</span> <code class="font-weight-bold"><?php echo !is_null($tabel_barang) ? count($tabel_barang) : 0; ?></code>
                  </p>
                </div>
              </div>
            </div>
            <?php if (!is_null($tabel_barang)) { ?>
            <div class="col-lg-12 pt-5">
              <div class="page-title">
                <h3 class="mb-0">Tabel Barang</h3>
              </div>
              <div class="card">
                <div class="card-body">
                  <div class="filter">
                    <div class="input-group input-group-sm" style="width:250px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-search"></i>
                        </span>
                      </div>
                      <input type="text" name="cari" class="form-control" placeholder="Cari barang" id="cari-tabel" onkeyup="cariTabel();" autofocus>
                    </div>
                    <div class="input-group input-group-sm" style="width:150px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-arrow-down"></i>
                        </span>
                      </div>
                      <select name="urutan" class="form-control" onchange="urutanTabel(this.value);">
                        <option value="1">Nama</option>
                        <option value="2">Alias</option>
                        <option value="3">Tipe</option>
                        <option value="4">Harga</option>
                      </select>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped" id="tabel-data">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Alias</th>
                          <th>Tipe</th>
                          <th>Harga</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="tbody">
                        <?php
                          if (!is_null($tabel_barang)) {
                            $nomor = 1;
                            foreach ($tabel_barang as $barang) {
                        ?>
                        <tr>
                          <td><?php echo $nomor++; ?>.</td>
                          <td><?php echo $barang->nama_barang; ?></td>
                          <td><?php echo $barang->alias; ?></td>
                          <td><?php echo $barang->tipe; ?></td>
                          <td><?php echo $this->fungsi->harga($barang->harga); ?></td>
                          <td align="right">
                            <a class="ml-2 text-primary" href="<?php echo base_url("barang/detail/" . $barang->id); ?>"><i class="icon-file-text menu-icon"></i> Detail</a>
                          </td>
                        </tr>
                        <?php
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

