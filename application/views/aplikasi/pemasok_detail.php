<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">
                  <a href="<?php echo base_url('pemasok'); ?>">Tabel Pemasok</a>
                  <i class="menu-icon icon-chevron-right"></i>
                  Detail
                  <i class="menu-icon icon-chevron-right"></i>
                  <?php echo $detail_pemasok->nama; ?>
                </h3>
                <a href="<?php echo base_url('pemasok/ubah/' . $detail_pemasok->id); ?>" class="btn btn-success btn-icon-text btn-sm">
                  <i class="menu-icon icon-edit-2 btn-icon-prepend"></i>
                  Ubah
                </a>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $detail_pemasok->nama; ?></h4>
                  <p class="card-description">
                    <span class="card-label">Kode</span> <code class="font-weight-bold"><?php echo $detail_pemasok->kode; ?></code>
                    <br/>
                    <span class="card-label">Kota</span> <code class="font-weight-bold"><?php echo $detail_pemasok->kota; ?></code>
                  </p>
                  <address>
                    <p class="font-weight-bold">Alamat</p>
                    <p>
                      <?php echo $detail_pemasok->alamat; ?>
                    </p>
                    <p>
                      <?php echo $detail_pemasok->kota; ?>
                    </p>
                  </address>
                  <address class="text-primary">
                    <p class="font-weight-bold">Kontak</p>
                    <p>
                      <?php echo $detail_pemasok->telepon; ?>
                    </p>
                    <p>
                      <?php echo $detail_pemasok->kontak; ?>
                    </p>
                  </address>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

