<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">Tabel Akun</h3>
                <a href="<?php echo base_url('akun/tambah'); ?>" class="btn btn-info btn-icon-text btn-sm">
                  <i class="menu-icon icon-plus-circle btn-icon-prepend"></i>
                  Tambah
                </a>
              </div>
              <div class="card">
                <div class="card-body">
                  <div class="filter">
                    <div class="input-group input-group-sm" style="width:250px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-search"></i>
                        </span>
                      </div>
                      <input type="text" name="cari" class="form-control" placeholder="Cari akun" id="cari-tabel" onkeyup="cariTabel();" autofocus>
                    </div>
                    <div class="input-group input-group-sm" style="width:150px;">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="menu-icon icon-arrow-down"></i>
                        </span>
                      </div>
                      <select name="urutan" class="form-control" onchange="urutanTabel(this.value);">
                        <option value="2">Nama</option>
                        <option value="3">Email</option>
                        <option value="4">HP</option>
                      </select>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped" id="tabel-data">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Akun</th>
                          <th>Nama</th>
                          <th>Email</th>
                          <th>HP</th>
                          <th>Login</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="tbody">
                        <?php
                          if (!is_null($tabel_akun)) {
                            $nomor = 1;
                            foreach ($tabel_akun as $akun) {
                        ?>
                        <tr>
                          <td><?php echo $nomor++; ?>.</td>
                          <td class="py-1">
                            <img src="<?php echo !is_null($akun->foto) ? base_url('assets/images/akun/' . $akun->foto) : base_url('assets/images/avatar.jpg'); ?>" alt="Foto <?php echo $akun->nama; ?>"/>
                          </td>
                          <td><?php echo $akun->nama; ?></td>
                          <td><?php echo $akun->email; ?></td>
                          <td><?php echo $akun->hp; ?></td>
                          <td><?php echo !is_null($akun->login) ? date('d/m/Y H:i:s', strtotime($akun->login)) : ''; ?></td>
                          <td align="right">
                            <a class="ml-2 text-success" href="<?php echo base_url("akun/ubah/" . $akun->id); ?>"><i class="icon-edit-3 menu-icon"></i> Ubah</a>
                          </td>
                        </tr>
                        <?php
                            }
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

