<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('komponen/css'); ?>
<body>
  <div class="container-scroller">
    <?php $this->load->view('komponen/navigasi'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('komponen/menu'); ?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-title">
                <h3 class="mb-0">
                  <a href="<?php echo base_url('pemasok'); ?>">Tabel Pemasok</a>
                  <i class="menu-icon icon-chevron-right"></i>
                  Tambah
                </h3>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <form class="forms-sample" method="post" action="<?php echo base_url('aksi/pemasok/tambah'); ?>">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" name="nama" class="form-control" required>
                    </div>
                    <div class="form-group">
                      <label>Kode</label>
                      <input type="text" name="kode" class="form-control" required>
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="alamat" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Kota</label>
                      <input type="text" name="kota" class="form-control" required>
                    </div>
                    <div class="form-group">
                      <label>Telepon</label>
                      <input type="text" name="telepon" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Kontak</label>
                      <input type="text" name="kontak" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-info mr-2">Simpan</button>
                    <a href="<?php echo base_url('pemasok'); ?>" class="btn btn-light">Kembali</a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
  </div>
  <?php $this->load->view('komponen/js'); ?>
</body>
</html>

