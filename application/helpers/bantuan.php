<?php
defined('BASEPATH') or exit('No direct script access allowed');

function media_url($path = null)
{
    return base_url('assets/img/' . $path);
}

function toDate($string, $type = 'j F Y')
{
    return !is_null($string) ? date($type, strtotime($string)) : null;
}

function toTime($string, $type = 'H:i')
{
    return !is_null($string) ? date($type, strtotime(date('Y-m-d ') . $string)) : null;
}

function toMinute($time, $type = 'time')
{
    if ($type == 'time') {
        $time = explode(':', $time);
        return round(($time[0]*60) + ($time[1]) + ($time[2]/60), 0);
    }
}

function toPrice($string, $currency = 'IDR')
{
    $number = preg_replace('/\D/', '', $string);
    $price  = number_format($number, 0, ',', '.');
    if (is_null($currency)) {
        return $price;
    }
    return $currency . ' ' . $price;
}

function toNumber($string)
{
    preg_match_all('!\d+!', $string, $matches);
    $number = (!empty(@$matches) && !empty(@$matches[0])) ? implode("", $matches[0]) : 0;
    return $number;
}

function getRandom($length = 8, $type = 'alphanumeric')
{    
    $random = "";
    switch ($type) {
        case 'alpha':  
            $pattern = "abcdefghijklmnopqrstuvwxyz";
            break;
        case 'ALPHA':  
            $pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            break;
        case 'numeric':
            $pattern = "0123456789";
            break;
        case 'alphanumeric':
            $pattern = "abcdefghijklmnopqrstuvwxyz0123456789";
            break;
        default:
            $pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            break;
    }
    for ($i = 0; $i < $length; $i++) {
        $random .= $pattern[mt_rand(0, strlen($pattern) - 1)];
    }
    return $random;
}

function nl2p($text)
{
    return '<p>* ' . str_replace(array("\r\n", "\r", "\n"), '</p><p>* ', $text) . '</p>';
}
