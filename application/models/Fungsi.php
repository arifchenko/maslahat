<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fungsi extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cek_sesi()
    {
        if ($this->session->akun) {
            $sesi = $this->session->akun;
            $sesi = json_encode($sesi);
            $sesi = json_decode($sesi);
            return $sesi;
        }
        redirect(base_url('login'));
    }

    public function buat_pesan($tipe, $pesan)
    {
        $this->session->set_flashdata($tipe, $pesan);
    }

    public function ambil_pesan()
    {
        if ($pesan = $this->session->flashdata()) {
            $tipe = array_keys($pesan);
            echo
                '<div class="alert alert-' . $tipe[0] . '">' .
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="faw fa-times"></i></button>' .
                '<span>' . $pesan[$tipe[0]] . '</span>' .
                '</div>';
        }
    }

    public function kirim_email($email, $judul, $pesan)
    {
        $konfigurasi = array(
            'smtp_host'      => 'smtp.gmail.com',
            'smtp_port'      => '587',
            'smtp_user'      => 'bengkel.maslahat@gmail.com',
            'smtp_pass'      => 'Ciganitri15!',
            'smtp_crypto'    => 'tls',
            '_smtp_auth'     => true,
            'protocol'       => 'smtp',
            'mailtype'       => 'html',
            'send_multipart' => false,
            'charset'        => 'utf-8',
            'wordwrap'       => true,
            'crlf'           => "\r\n",
            'newline'        => "\r\n",
        );

        $this->load->library('email');
        $this->email->initialize($konfigurasi);
        $this->email->from('bengkel.maslahat@gmail.com', "Maslahat Motor");
        $this->email->to($email);
        $this->email->subject($judul);
        $this->email->message($pesan);
        $this->email->send();
        return $this->email->print_debugger();
    }

    public function akses()
    {
        $ip = "125.161.218.200";
        $lokasi = "Bandung";
        $provinsi = "Jawa Barat";
        $cek_ip = "http://l2.io/ip.js?var=myip";
        $cari = file_get_contents($cek_ip);
        if (preg_match('/"([^"]+)"/', $cari, $hasil)) {
            $ip = $hasil[1];
            $cek_lokasi = "https://freegeoip.app/json/" . $ip;
            $link = file_get_contents($cek_lokasi);
            $konten = json_decode($link);
            $lokasi = $konten->city;
            $provinsi = $konten->region_name;
        }

        $cuaca = "25°C";
        $cek_cuaca = "http://www.google.com/search?q=weather%20" . $lokasi;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $cek_cuaca);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $page = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        echo $code;

        // list($status_cuaca) = get_headers($cek_cuaca);
        // if (strpos($status_cuaca, '200') !== FALSE) {
        //     $konten = file_get_contents($cek_cuaca);
        //     $dom = new DOMDocument();
        //     libxml_use_internal_errors(true);
        //     $dom->loadHTML($konten);
        //     libxml_use_internal_errors(false);
        //     $xpath = new DOMXPath($dom);
        //     $div = $xpath->query('//div[@class="BNeawe iBp4i AP7Wnd"]');
        //     $div = $div->item(1);
        //     $cuaca = $dom->saveXML($div);
        // }

        $browser = $this->agent->browser();
        $perangkat = $this->agent->is_mobile() ? 'HP' : 'Komputer';
        $versi = $this->agent->platform();
        $data = array(
            'ip' => $ip,
            'lokasi' => $lokasi,
            'provinsi' => $provinsi,
            'cuaca' => strip_tags($cuaca),
            'browser' => $browser,
            'perangkat' => $perangkat,
            'versi' => $versi,
        );
        return (object) $data;
    }

    public function waktu() {
        $jam = date("H");
        if (($jam > 6) && ($jam < 18)) {
            $kondisi = 'Siang';
            $latar = base_url('assets/images/day-time.jpg');
        } else {
            $kondisi = 'Malam';
            $latar = base_url('assets/images/night-time.jpg');
        }
        $data = array(
            'kondisi' => $kondisi,
            'latar' => $latar,
        );
        return (object) $data;
    }

    public function harga($nominal)
    {
        $nominal = preg_replace('/\D/', '', $nominal);
        $harga  = number_format($nominal, 0, ',', '.');
        return "Rp " . $harga;
    }
}
