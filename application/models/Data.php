<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Model
{

    /* Database */

    public function ambil($tabel, $kondisi = '', $tampil = '*')
    {
        $perintah = $this->db->query("SELECT " . $tampil . " FROM " . $tabel . " " . $kondisi);
        return $perintah->num_rows() ? $perintah->result() : null;
    }

    public function tambah($tabel, $data)
    {
        $this->db->insert($tabel, $data);
        return $this->ambil($tabel, "WHERE id = '" . $this->db->insert_id() . "'");
    }

    public function ubah($tabel, $data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($tabel, $data);
        $hasil = $this->ambil($tabel, "WHERE id = '" . $id . "'");
        return $hasil[0];
    }

    public function hapus($tabel, $id)
    {
        $hasil = $this->ambil($tabel, "WHERE id = '" . $id . "'");
        $this->db->where("id", $id);
        $this->db->delete($tabel);
        return $hasil[0];
    }

    /* Barang */

    public function tabel_barang_dari_kategori($id_kategori)
    {
        $tabel_barang = $this->ambil('barang', "
            JOIN kategori ON kategori.id = barang.id_kategori
            WHERE id_kategori = $id_kategori
            ORDER BY barang.nama
        ", "
            *,
            barang.id AS id,
            barang.nama AS nama_barang,
            kategori.id AS id_kategori,
            kategori.nama AS nama_kategori
        ");
        if (!is_null($tabel_barang)) {
            return $tabel_barang;
        } else {
            return null;
        }
    }

    /* Kategori */

    public function tabel_kategori()
    {
        $tabel_kategori = $this->ambil('kategori', "
            LEFT JOIN barang ON barang.id_kategori = kategori.id
            GROUP BY kategori.id
            ORDER BY nama
        ", "
            kategori.*,
            COUNT(barang.id) AS jumlah
        ");
        if (!is_null($tabel_kategori)) {
            return $tabel_kategori;
        } else {
            return null;
        }
    }

    /* Pemasok */

    public function tabel_pemasok()
    {
        $tabel_pemasok = $this->ambil('pemasok', "ORDER BY nama");
        if (!is_null($tabel_pemasok)) {
            return $tabel_pemasok;
        } else {
            return null;
        }
    }

    /* Akun */

    public function cek_akun($username, $password)
    {
        $tabel_akun = $this->ambil('akun', "WHERE username = '" . $username . "' AND password = '" . $password . "'");
        if (!is_null($tabel_akun)) {
            $akun = $tabel_akun[0];

            return $akun;
        } else {
            return null;
        }
    }

    public function cek_lupa($email)
    {
        $tabel_akun = $this->ambil('akun', "WHERE email = '" . $email . "'");
        if (!is_null($tabel_akun)) {
            $akun = $tabel_akun[0];

            return $akun;
        } else {
            return null;
        }
    }

    public function tabel_akun()
    {
        $tabel_akun = $this->ambil('akun', "ORDER BY nama");
        if (!is_null($tabel_akun)) {
            return $tabel_akun;
        } else {
            return null;
        }
    }
}
