<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aksi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /* Login */

    public function cek_login()
    {
        $username = $this->input->post('username');
        $username = trim($username);
        $username = strtolower($username);

        $password = $this->input->post('password');
        $password = trim($password);
        $password = md5($password);

        $akun = $this->data->cek_akun($username, $password);
        if (!is_null($akun)) {
            $sesi = array(
                'id'   => $akun->id,
                'nama' => $akun->nama,
                'foto' => $akun->foto,
            );
            $this->session->set_userdata('akun', $sesi);

            $data = array(
                'login' => date('Y-m-d H:i:s'),
            );
            $this->data->ubah('akun', $data, $akun->id);

            redirect(base_url());
        } else {
            redirect(base_url('login'));
        }
    }

    public function cek_lupa()
    {
        $email = $this->input->post('email');
        $email = trim($email);
        $email = strtolower($email);

        $akun = $this->data->cek_lupa($email);
        if (!is_null($akun)) {
            $email    = $akun->email;
            $password = substr(uniqid(mt_rand()), 0, 4);
            $judul    = "Aplikasi Maslahat Motor: Password Baru";
            $pesan    = "Password baru anda adalah: " . $password;
            $this->fungsi->kirim_email($email, $judul, $pesan);

            $id   = $akun->id;
            $data = array(
                'password' => md5($password),
            );
            $this->data->ubah('akun', $data, $id);

            redirect(base_url('login'));
        } else {
            redirect(base_url('lupa'));
        }
    }

    /* Profil */

    public function profil_ubah()
    {
        $akun   = $this->fungsi->cek_sesi();
        $profil = $this->data->ambil('akun', "WHERE id = '" . $akun->id . "'");

        $konfigurasi = array(
            'upload_path'   => './assets/images/akun/',
            'allowed_types' => 'gif|jpg|png',
        );

        $this->load->library('upload');
        $this->upload->initialize($konfigurasi);

        if ($this->upload->do_upload('foto')) {
            $upload = $this->upload->data();
            $foto   = $profil[0]->id . $upload['file_ext'];
            rename($konfigurasi['upload_path'] . $upload['file_name'], $konfigurasi['upload_path'] . $foto);
        } else {
            $foto = $profil[0]->foto;
        }

        $nama = $this->input->post('nama');
        $nama = trim($nama);

        $email = $this->input->post('email');
        $email = trim($email);
        $email = strtolower($email);

        $hp = $this->input->post('hp');
        $hp = trim($hp);

        $username = $this->input->post('username');
        $username = trim($username);
        $username = strtolower($username);

        $password = $this->input->post('password');
        if ($password) {
            $password = trim($password);
            $password = md5($password);
        } else {
            $password = $profil[0]->password;
        }

        $data = array(
            'nama'     => $nama,
            'email'    => $email,
            'hp'       => $hp,
            'username' => $username,
            'password' => $password,
            'foto'     => $foto,
        );
        $this->data->ubah('akun', $data, $akun->id);

        $sesi = array(
            'id'   => $akun->id,
            'nama' => $nama,
            'foto' => $foto,
        );
        $this->session->set_userdata('akun', $sesi);

        redirect(base_url('profil'));
    }

    /* Kategori */

    public function kategori_tambah()
    {
        $akun = $this->fungsi->cek_sesi();

        $nama = $this->input->post('nama');
        $nama = trim($nama);
        $nama = strtoupper($nama);

        $cek = $this->data->ambil('kategori', "WHERE nama = '$nama'");
        if (is_null($cek)) {
            $data = array(
                'nama'    => $nama,
            );
            $this->data->tambah('kategori', $data);

            redirect(base_url('kategori'));
        } else {
            redirect(base_url('kategori/tambah'));
        }
    }

    public function kategori_ubah()
    {
        $akun = $this->fungsi->cek_sesi();

        $id = $this->input->post('id');

        $nama = $this->input->post('nama');
        $nama = trim($nama);
        $nama = strtoupper($nama);

        $cek = $this->data->ambil('kategori', "WHERE id <> '$id' AND nama = '$nama'");
        if (is_null($cek)) {
            $data = array(
                'nama'    => $nama,
            );
            $this->data->ubah('kategori', $data, $id);

            redirect(base_url('kategori'));
        } else {
            redirect(base_url('kategori/ubah/' . $id));            
        }
    }

    /* Pemasok */

    public function pemasok_tambah()
    {
        $akun = $this->fungsi->cek_sesi();

        $nama = $this->input->post('nama');
        $nama = trim($nama);
        $nama = strtoupper($nama);

        $kode = $this->input->post('kode');
        $kode = trim($kode);
        $kode = strtoupper($kode);

        $alamat = $this->input->post('alamat');
        $alamat = trim($alamat);
        $alamat = strtoupper($alamat);

        $kota = $this->input->post('kota');
        $kota = trim($kota);
        $kota = strtoupper($kota);

        $telepon = $this->input->post('telepon');
        $telepon = trim($telepon);

        $kontak = $this->input->post('kontak');
        $kontak = trim($kontak);
        $kontak = strtoupper($kontak);

        $cek = $this->data->ambil('pemasok', "WHERE kode = '$kode' OR (nama = '$nama' AND kota = '$kota')");
        if (is_null($cek)) {
            $data = array(
                'nama'    => $nama,
                'kode'    => $kode,
                'alamat'  => $alamat,
                'kota'    => $kota,
                'telepon' => $telepon,
                'kontak'  => $kontak,
            );
            $this->data->tambah('pemasok', $data);

            redirect(base_url('pemasok'));
        } else {
            redirect(base_url('pemasok/tambah'));
        }
    }

    public function pemasok_ubah()
    {
        $akun = $this->fungsi->cek_sesi();

        $id = $this->input->post('id');

        $nama = $this->input->post('nama');
        $nama = trim($nama);
        $nama = strtoupper($nama);

        $kode = $this->input->post('kode');
        $kode = trim($kode);
        $kode = strtoupper($kode);

        $alamat = $this->input->post('alamat');
        $alamat = trim($alamat);
        $alamat = strtoupper($alamat);

        $kota = $this->input->post('kota');
        $kota = trim($kota);
        $kota = strtoupper($kota);

        $telepon = $this->input->post('telepon');
        $telepon = trim($telepon);

        $kontak = $this->input->post('kontak');
        $kontak = trim($kontak);
        $kontak = strtoupper($kontak);

        $cek = $this->data->ambil('pemasok', "WHERE id <> '$id' AND (kode = '$kode' OR (nama = '$nama' AND kota = '$kota'))");
        if (is_null($cek)) {
            $data = array(
                'nama'    => $nama,
                'kode'    => $kode,
                'alamat'  => $alamat,
                'kota'    => $kota,
                'telepon' => $telepon,
                'kontak'  => $kontak,
            );
            $this->data->ubah('pemasok', $data, $id);

            redirect(base_url('pemasok'));
        } else {
            redirect(base_url('pemasok/ubah/' . $id));            
        }
    }

    /* Akun */

    public function akun_tambah()
    {
        $akun = $this->fungsi->cek_sesi();

        $nama = $this->input->post('nama');
        $nama = trim($nama);

        $email = $this->input->post('email');
        $email = trim($email);
        $email = strtolower($email);

        $hp = $this->input->post('hp');
        $hp = trim($hp);

        $username = $this->input->post('username');
        $username = trim($username);
        $username = strtolower($username);

        $password = $this->input->post('password');
        $password = trim($password);
        $password = md5($password);

        $cek = $this->data->ambil('akun', "WHERE email = '$email' OR username = '$username'");
        if (is_null($cek)) {
            $data = array(
                'nama'     => $nama,
                'email'    => $email,
                'hp'       => $hp,
                'username' => $username,
                'password' => $password,
            );
            $this->data->tambah('akun', $data);

            redirect(base_url('akun'));
        } else {
            redirect(base_url('akun/tambah'));
        }
    }

    public function akun_ubah()
    {
        $akun = $this->fungsi->cek_sesi();

        $id = $this->input->post('id');

        $nama = $this->input->post('nama');
        $nama = trim($nama);

        $email = $this->input->post('email');
        $email = trim($email);
        $email = strtolower($email);

        $hp = $this->input->post('hp');
        $hp = trim($hp);

        $data = array(
            'nama'  => $nama,
            'email' => $email,
            'hp'    => $hp,
        );
        $this->data->ubah('akun', $data, $id);

        redirect(base_url('akun'));
    }
}
