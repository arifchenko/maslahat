<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aplikasi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function error()
    {
        $this->load->view('aplikasi/error');
    }

    public function index()
    {
        $akun  = $this->fungsi->cek_sesi();
        $akses = $this->fungsi->akses();
        $waktu = $this->fungsi->waktu();
        $data  = array(
            'akun'  => $akun,
            'akses' => $akses,
            'waktu' => $waktu,
        );
        $this->load->view('aplikasi/index', $data);
    }

    /* Login */

    public function login()
    {
        $this->session->unset_userdata('akun');
        $this->load->view('aplikasi/login');
    }

    public function logout()
    {
        $this->session->unset_userdata('akun');
        $this->load->view('aplikasi/login');
    }

    public function lupa()
    {
        $this->session->unset_userdata('akun');
        $this->load->view('aplikasi/lupa');
    }

    /* Profil */

    public function profil()
    {
        $akun   = $this->fungsi->cek_sesi();
        $profil = $this->data->ambil('akun', "WHERE id = '" . $akun->id . "'");
        $data   = array(
            'akun'   => $akun,
            'profil' => $profil[0],
        );
        $this->load->view('aplikasi/profil', $data);
    }

    /* Kategori */

    public function kategori()
    {
        $akun           = $this->fungsi->cek_sesi();
        $tabel_kategori = $this->data->tabel_kategori();
        $data           = array(
            'akun'           => $akun,
            'tabel_kategori' => $tabel_kategori,
        );
        $this->load->view('aplikasi/kategori', $data);
    }

    public function kategori_tambah()
    {
        $akun = $this->fungsi->cek_sesi();
        $data = array(
            'akun' => $akun,
        );
        $this->load->view('aplikasi/kategori_tambah', $data);
    }

    public function kategori_ubah($id)
    {
        $akun          = $this->fungsi->cek_sesi();
        $ubah_kategori = $this->data->ambil('kategori', "WHERE id = '" . $id . "'");
        $data          = array(
            'akun'          => $akun,
            'ubah_kategori' => $ubah_kategori[0],
        );
        $this->load->view('aplikasi/kategori_ubah', $data);
    }

    public function kategori_detail($id)
    {
        $akun            = $this->fungsi->cek_sesi();
        $detail_kategori = $this->data->ambil('kategori', "WHERE id = '" . $id . "'");
        $tabel_barang    = $this->data->tabel_barang_dari_kategori($id);
        $data            = array(
            'akun'            => $akun,
            'detail_kategori' => $detail_kategori[0],
            'tabel_barang'    => $tabel_barang,
        );
        $this->load->view('aplikasi/kategori_detail', $data);
    }

    /* Pemasok */

    public function pemasok()
    {
        $akun          = $this->fungsi->cek_sesi();
        $tabel_pemasok = $this->data->tabel_pemasok();
        $data          = array(
            'akun'          => $akun,
            'tabel_pemasok' => $tabel_pemasok,
        );
        $this->load->view('aplikasi/pemasok', $data);
    }

    public function pemasok_tambah()
    {
        $akun = $this->fungsi->cek_sesi();
        $data = array(
            'akun' => $akun,
        );
        $this->load->view('aplikasi/pemasok_tambah', $data);
    }

    public function pemasok_ubah($id)
    {
        $akun         = $this->fungsi->cek_sesi();
        $ubah_pemasok = $this->data->ambil('pemasok', "WHERE id = '" . $id . "'");
        $data         = array(
            'akun'         => $akun,
            'ubah_pemasok' => $ubah_pemasok[0],
        );
        $this->load->view('aplikasi/pemasok_ubah', $data);
    }

    public function pemasok_detail($id)
    {
        $akun           = $this->fungsi->cek_sesi();
        $detail_pemasok = $this->data->ambil('pemasok', "WHERE id = '" . $id . "'");
        $data           = array(
            'akun'           => $akun,
            'detail_pemasok' => $detail_pemasok[0],
        );
        $this->load->view('aplikasi/pemasok_detail', $data);
    }

    /* Akun */

    public function akun()
    {
        $akun       = $this->fungsi->cek_sesi();
        $tabel_akun = $this->data->tabel_akun();
        $data       = array(
            'akun'       => $akun,
            'tabel_akun' => $tabel_akun,
        );
        $this->load->view('aplikasi/akun', $data);
    }

    public function akun_tambah()
    {
        $akun = $this->fungsi->cek_sesi();
        $data = array(
            'akun' => $akun,
        );
        $this->load->view('aplikasi/akun_tambah', $data);
    }

    public function akun_ubah($id)
    {
        $akun      = $this->fungsi->cek_sesi();
        $ubah_akun = $this->data->ambil('akun', "WHERE id = '" . $id . "'");
        $data      = array(
            'akun'      => $akun,
            'ubah_akun' => $ubah_akun[0],
        );
        $this->load->view('aplikasi/akun_ubah', $data);
    }
}
