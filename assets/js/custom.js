if ($("#display-datetime").length) {
  var timeDisplay = document.getElementById("display-datetime");
  function refreshTime() {
    var date_option = {
      timeZone: "Asia/Jakarta",
      weekday: "long",
      day: "2-digit",
      month: "long",
      year: "numeric",
    }
    var time_option = {
      hour: "2-digit", 
      minute: "2-digit",
      second: "2-digit"
    }
    var dateString = new Date().toLocaleString("id-ID", date_option);
    var timeString = new Date().toLocaleString("id-ID", time_option);
    timeString = timeString.replace(".", ":");
    timeString = timeString.replace(".", ":");
    timeDisplay.innerHTML = dateString + " - " + timeString;
  }
  setInterval(refreshTime, 1000);
}

function cariTabel() {
  var input, table, tr, td, i, j, count, txtValue;
  input = document.getElementById("cari-tabel").value.toUpperCase();
  table = document.getElementById("tabel-data");
  tr = table.getElementsByTagName("tr");
  count = document.getElementById("tabel-data").rows[1].cells.length;
  for (i = 1; i < tr.length; i++) {
    txtValue = "";
    for (j = 0; j < count; j++) {
      td = tr[i].getElementsByTagName("td")[j];
      if(td) {
        txtValue += (td.textContent || td.innerText) + " ";
      }
    }
    if (txtValue.toUpperCase().indexOf(input) > -1) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  } 
}

function urutanTabel(kolom) {
  var tableData = document.getElementById("tabel-data").getElementsByTagName('tbody').item(0);
  var rowData = tableData.getElementsByTagName('tr');            
  for(var i = 0; i < rowData.length - 1; i++) {
    for(var j = 0; j < rowData.length - (i + 1); j++) {
      if(Number(rowData.item(j).getElementsByTagName('td').item(kolom).innerHTML.replace(/[^0-9\.]+/g, "")) < Number(rowData.item(j+1).getElementsByTagName('td').item(kolom).innerHTML.replace(/[^0-9\.]+/g, ""))) {
        tableData.insertBefore(rowData.item(j+1),rowData.item(j));
      }
    }
  }
}

var index;
function urutanTabel(index) {
  this.index = index;
  var tbody = document.getElementById("tabel-data").getElementsByTagName('tbody').item(0);
  var datas = new Array();
  var tbodyLength = tbody.rows.length;
  for (var i = 0; i < tbodyLength; i++) {
    datas[i] = tbody.rows[i];
  }
  datas.sort(compareCells);
  for (var i = 0; i < tbody.rows.length; i++) {
    tbody.appendChild(datas[i]);
  }   
}

function compareCells(a,b) {
  var aVal = a.cells[index].innerText;
  var bVal = b.cells[index].innerText;
  aVal = aVal.replace(/\,/g, '');
  bVal = bVal.replace(/\,/g, '');
  if (aVal.match(/^[0-9]+$/) && bVal.match(/^[0-9]+$/)) {
    return parseFloat(aVal) - parseFloat(bVal);
  }
  else {
    if (aVal < bVal) {
      return -1; 
    } else if (aVal > bVal) {
      return 1; 
    } else {
      return 0;       
    }         
  }
}